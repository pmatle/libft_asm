/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 08:52:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 15:43:47 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

int			main(void)
{
	char	*str1;
	char	*str2;

	str1 = (char *)malloc(sizeof(*str1) * 12); 
	str2 = (char *)malloc(sizeof(*str2) * 12); 
	ft_puts("| ***************************************************** |");
	ft_puts("|                   Testing ft_strlen                   |");
	ft_puts("| ***************************************************** |");
	ft_puts("\n");
	str1 = ft_memcpy(str1, "Hello World", 11);
	str2 = memcpy(str2, "Hello World", 11);
	printf("Original : %zu\n", strlen(str1));
	printf("Mine : %zu\n\n", ft_strlen(str2));
	str1 = ft_strcat(str1, " Phuti here");
	str2 = strcat(str2, " Phuti here");
	printf("Original : %zu\n", strlen(str1));
	printf("Mine : %zu\n\n", ft_strlen(str2));
	str1 = ft_strcat(str1, " , telling a story like no other");
	str2 = strcat(str2, " , telling a story like no other");
	printf("Original : %zu\n", strlen(str1));
	printf("Mine : %zu\n\n", ft_strlen(str2));
	printf("Original : %zu\n", strlen("Someday my friend"));
	printf("Mine : %zu\n\n", ft_strlen("Someday my friend"));
	free(str1);
	free(str2);
	ft_puts("\n| ********************* END OF TEST ******************* |");
	return (0);
}

