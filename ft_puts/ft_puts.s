section		.data
variable:
	.newline db  10
	.null db '(null)'

section		.text
	global	_ft_puts

_ft_puts:

	mov		r8, 0

check_null:
	cmp		rdi, 0
	jne		count_chars
	lea		rdi, [rel variable.null]
	mov		r8, 6
	jmp		write_string

count_chars:
	cmp		byte [rdi + r8], 0
	je		write_string
	inc		r8
	jmp		count_chars

write_string:
	mov		rdx, r8
	mov		rsi, rdi
	mov		rdi, 1
	mov		rax, 0x2000004
	syscall

write_newline:
	mov		rdx, 1
	lea		rsi, [rel variable.newline]
	mov		rdi, 1
	mov		rax, 0x2000004
	syscall

cleanup:
	mov		rax, r8
	ret

