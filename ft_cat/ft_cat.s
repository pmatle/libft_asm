extern		_ft_puts

section		.data
variable:
	.error db "Unable to read from file descriptor", 0

section		.text
	global	_ft_cat

_ft_cat:
	mov		r8, rdi

check_neg_fd:
	test	edi, edi
	js		failed

read_from_fd:
	mov		rax, 0x2000003
	mov		rdi, r8
	mov		rsi, buff
	mov		rdx, 4096
	syscall
	jc		failed
	test	eax, eax
	jz		end


write_string:
	mov		rdx, rax
	mov		rax, 0x2000004
	mov		rdi, 1
	syscall
	jmp		read_from_fd

end:
	ret

failed:
	lea		rdi, [rel variable.error]
	call	_ft_puts
	ret

section		.bss
	buff resb	4096
