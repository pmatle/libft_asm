/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 09:47:50 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 09:21:41 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

int			main(void)
{
	ft_puts("| ***************************************************** |");
	ft_puts("|                     Testing ft_cat.s                  |");
	ft_puts("| ***************************************************** |");
	ft_puts("\n\n");
	ft_puts("\tTesting with negative file descriptor\n");
	ft_cat(-19);
	ft_puts("\n\n");
	ft_puts("\tTesting with invalid file descriptor\n");
	ft_cat(14);
	ft_puts("\n| ******************** END OF TEST ******************** |");
	return (0);
}
