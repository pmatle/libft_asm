# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmatle <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/21 08:07:16 by pmatle            #+#    #+#              #
#    Updated: 2018/06/28 16:20:00 by pmatle           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libfts.a

OBJS_FOLDER = compiled

FUNC_FOLDERS = ft_bzero ft_

OBJS = compiled/ft_bzero.o compiled/ft_strcat.o compiled/ft_isalpha.o \
	   compiled/ft_isdigit.o compiled/ft_isalnum.o compiled/ft_isascii.o \
	   compiled/ft_isprint.o compiled/ft_toupper.o compiled/ft_tolower.o \
	   compiled/ft_puts.o compiled/ft_strlen.o compiled/ft_memset.o \
	   compiled/ft_memcpy.o compiled/ft_strdup.o compiled/ft_cat.o \
	   compiled/ft_swap_cp.o compiled/ft_swap_ip.o compiled/ft_swap_digits.o \
	   compiled/ft_putstr.o compiled/ft_memchr.o

ASM = nasm

ASMFLAGS = -f macho64

all: $(NAME)

$(NAME):
	@mkdir $(OBJS_FOLDER)
	@$(ASM) $(ASMFLAGS) ft_bzero/ft_bzero.s -o compiled/ft_bzero.o
	@$(ASM) $(ASMFLAGS) ft_strcat/ft_strcat.s -o compiled/ft_strcat.o
	@$(ASM) $(ASMFLAGS) ft_is/ft_isalpha.s -o compiled/ft_isalpha.o
	@$(ASM) $(ASMFLAGS) ft_is/ft_isalnum.s -o compiled/ft_isalnum.o
	@$(ASM) $(ASMFLAGS) ft_is/ft_isdigit.s -o compiled/ft_isdigit.o
	@$(ASM) $(ASMFLAGS) ft_is/ft_isascii.s -o compiled/ft_isascii.o
	@$(ASM) $(ASMFLAGS) ft_is/ft_isprint.s -o compiled/ft_isprint.o
	@$(ASM) $(ASMFLAGS) ft_to/ft_toupper.s -o compiled/ft_toupper.o
	@$(ASM) $(ASMFLAGS) ft_to/ft_tolower.s -o compiled/ft_tolower.o
	@$(ASM) $(ASMFLAGS) ft_puts/ft_puts.s -o compiled/ft_puts.o
	@$(ASM) $(ASMFLAGS) ft_memset/ft_memset.s -o compiled/ft_memset.o
	@$(ASM) $(ASMFLAGS) ft_strlen/ft_strlen.s -o compiled/ft_strlen.o
	@$(ASM) $(ASMFLAGS) ft_memcpy/ft_memcpy.s -o compiled/ft_memcpy.o
	@$(ASM) $(ASMFLAGS) ft_strdup/ft_strdup.s -o compiled/ft_strdup.o
	@$(ASM) $(ASMFLAGS) ft_cat/ft_cat.s -o compiled/ft_cat.o
	@$(ASM) $(ASMFLAGS) ft_swap/ft_swap_digits.s -o compiled/ft_swap_digits.o
	@$(ASM) $(ASMFLAGS) ft_swap/ft_swap_cp.s -o compiled/ft_swap_cp.o
	@$(ASM) $(ASMFLAGS) ft_swap/ft_swap_ip.s -o compiled/ft_swap_ip.o
	@$(ASM) $(ASMFLAGS) ft_putstr/ft_putstr.s -o compiled/ft_putstr.o
	@$(ASM) $(ASMFLAGS) ft_memchr/ft_memchr.s -o compiled/ft_memchr.o
	@ar rcs $(NAME) $(OBJS)
	@echo "Library created successfully..."

tests:
	make -C ft_bzero
	make -C ft_is
	make -C ft_to
	make -C ft_puts
	make -C ft_putstr
	make -C ft_strdup
	make -C ft_memcpy
	make -C ft_memchr
	make -C ft_strlen
	make -C ft_memset
	make -C ft_cat
	make -C ft_swap

clean:
	@/bin/rm -rf $(OBJS_FOLDER)
	@echo "Object files cleaned successfully..."

fclean: clean
	@/bin/rm -rf $(NAME)
	@echo "library removed successfully..."

re: fclean all

