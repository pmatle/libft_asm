/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 08:52:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 09:16:43 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

void		character_array_test(void)
{
	char	*str;
	char	*bstr;

	str = (char *)malloc(sizeof(*str) * 12);
	bstr = (char *)malloc(sizeof(*bstr) * 12);
	ft_puts("\t\tCharacter array test : \n");
	ft_memcpy(str, "Hello World", 11);
	ft_memcpy(bstr, "Hello World", 11);
	ft_puts("\tBefore test : \n");
	ft_puts("Mine : ");
	ft_puts(str);
	ft_puts("\nOriginal : ");
	ft_puts(bstr);
	ft_puts("\n\tAfter test : \n");
	ft_bzero(str, 12);
	bzero(bstr, 12);
	ft_puts("\nMine : ");
	ft_puts(str);
	ft_puts("\nOriginal : ");
	ft_puts(bstr);
	free(str);
	free(bstr);
	ft_puts("| ************ END OF CHAR ARRAY TESTS  ************** |\n");
}

void		print_array(int *arr, int size)
{
	int		x;

	x = 0;
	while (x < size)
	{
		printf("%d\n", arr[x]);
		x++;
	}
	printf("\n");
}

void		int_array_test(int *arr, int *barr)
{
	ft_puts("\n\t\tInt array test : \n");
	ft_puts("\tBefore test : \n");
	ft_puts("Mine : ");
	print_array(arr, 5);
	ft_puts("Original : ");
	print_array(barr, 5);
	ft_puts("\tAfter test");
	ft_bzero(arr, (sizeof(int) * 5));
	bzero(barr, (sizeof(int) * 5));
	ft_puts("Mine : ");
	print_array(arr, 5);
	ft_puts("Original : ");
	print_array(barr, 5);
	ft_puts("| ************ END OF INT ARRAY TESTS  ************** |\n");
}

int			main(void)
{
	int		*arr;
	int		*barr;

	arr = (int *)malloc(sizeof(*arr) * 5);
	barr = (int *)malloc(sizeof(*barr) * 5);
	ft_puts("| ***************************************************** |");
	ft_puts("|                   Testing ft_bzero.s                  |");
	ft_puts("| ***************************************************** |\n");
	character_array_test();
	arr[0] = 12;
	arr[1] = 2;
	arr[2] = 1;
	arr[3] = 13;
	arr[4] = 145;
	barr[0] = 12;
	barr[1] = 2;
	barr[2] = 1;
	barr[3] = 13;
	barr[4] = 145;
	int_array_test(arr, barr);
	free(arr);
	free(barr);
	ft_puts("\n| ******************* END OF TESTS ******************** |");
	return (0);
}
