extern		_ft_memset

section		.text
	global	_ft_bzero

_ft_bzero:
	mov		rdx, rsi
	mov		rsi, 0
	call	_ft_memset

end_ft_bzero:
	mov		rax, rdi
	ret

