
section		.text
	global	_ft_swap_cp

_ft_swap_cp:
	mov		ecx, [rdi]
	mov		edx, [rsi]
	mov		[rsi], ecx
	mov		[rdi], edx
	ret
