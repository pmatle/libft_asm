
section		.text
	global	_ft_swap_ip

_ft_swap_ip:
	mov		ecx, [rdi]
	mov		edx, [rsi]
	mov		[rsi], ecx
	mov		[rdi], edx
	ret
