/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/27 07:32:20 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 16:05:02 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

void	primitive_tests(void)
{
	int		x;
	int		y;

	ft_puts("\tTesting with ints : \n");
	x = 3;
	y = 8;
	printf("Before : x = %d  y = %d\n", x, y);
	ft_swap_digits(&x, &y);
	printf("After : x = %d  y = %d\n\n", x, y);
	x = 29;
	y = 100;
	printf("Before : x = %d  y = %d\n", x, y);
	ft_swap_digits(&y, &x);
	printf("After : x = %d  y = %d\n\n", x, y);
}

void	print_array(int *arr, char *s1)
{
	int		x;

	x = 0;
	while (x < 5)
	{
		printf("%s[%d] : %d\n", s1, x, arr[x]);
		x++;
	}
}

void	int_array_test(int *intarr1, int *intarr2)
{
	intarr1[0] = 5;
	intarr1[1] = 53;
	intarr1[2] = 100;
	intarr1[3] = 24;
	intarr1[4] = 23;
	intarr2[0] = 12;
	intarr2[1] = 145;
	intarr2[2] = 75;
	intarr2[3] = 19;
	intarr2[4] = 70;
	ft_puts("\tTesting with int arrays : \n");
	ft_puts("\tBefore swapping : \n");
	print_array(intarr1, "intarr1");
	ft_putstr("\n");
	print_array(intarr2, "intarr2");
	ft_puts("\n");
	ft_swap_ip(&intarr1, &intarr2);
	ft_puts("\tAfter swapping : \n");
	print_array(intarr1, "intarr1");
	ft_putstr("\n");
	print_array(intarr2, "intarr2");
}

void	char_array_test(char *s1, char *s2)
{
	ft_puts("\tTesting with charecter pointers : \n");
	ft_memcpy(s1, "String 1", 8);
	ft_memcpy(s2, "String 2", 8);
	ft_puts("Before : \n");
	printf("s1 : %s\n", s1);
	printf("s2 : %s\n", s2);
	ft_puts("After : \n");
	ft_swap_cp(&s1, &s2);
	printf("s1 : %s\n", s1);
	printf("s2 : %s\n", s2);
}

int		main(void)
{
	int		*intarr1;
	int		*intarr2;
	char	*s1;
	char	*s2;

	intarr1 = (int *)malloc(sizeof(*intarr1) * 5);
	intarr2 = (int *)malloc(sizeof(*intarr2) * 5);
	s1 = (char *)malloc(sizeof(*s1) * 9);
	s2 = (char *)malloc(sizeof(*s1) * 9);
	ft_puts("| ********************************************************** |");
	ft_puts("|                     Testing ft_swap function               |");
	ft_puts("| ********************************************************** |\n");
	primitive_tests();
	int_array_test(intarr1, intarr2);
	char_array_test(s1, s2);
	free(s1);
	free(s2);
	free(intarr1);
	free(intarr2);
	ft_puts("\n| ******************* END OF TESTS ******************* |");
	return (0);
}
