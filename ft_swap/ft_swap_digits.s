
section		.text
	global	_ft_swap_digits

_ft_swap_digits:
	mov		ecx, [rdi]
	mov		edx, [rsi]
	mov		[rsi], ecx
	mov		[rdi], edx
	ret
