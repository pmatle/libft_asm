/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 08:52:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 16:42:48 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

void		ft_isalpha_test(void)
{
	ft_puts("\tft_isalpha tests : \n");
	printf("\tT     : %s\n", ft_isalpha('T') ? "true" : "false");
	printf("\tspace : %s\n", ft_isalpha(' ') ? "true" : "false");
	printf("\th     : %s\n", ft_isalpha('h') ? "true" : "false");
	printf("\t5     : %s\n", ft_isalpha('5') ? "true" : "false");
	printf("\t*     : %s\n\n", ft_isalpha('*') ? "true" : "false");
}

void		ft_isdigit_test(void)
{
	ft_puts("\tft_isdigit tests : \n");
	printf("\tT     : %s\n", ft_isdigit('T') ? "true" : "false");
	printf("\tspace : %s\n", ft_isdigit(' ') ? "true" : "false");
	printf("\t2     : %s\n", ft_isdigit('2') ? "true" : "false");
	printf("\t5     : %s\n", ft_isdigit('5') ? "true" : "false");
	printf("\t*     : %s\n\n", ft_isdigit('*') ? "true" : "false");
}

void		ft_isalnum_test(void)
{
	ft_puts("\tft_isalnum tests : \n");
	printf("\tT     : %s\n", ft_isalnum('T') ? "true" : "false");
	printf("\tspace : %s\n", ft_isalnum(' ') ? "true" : "false");
	printf("\th     : %s\n", ft_isalnum('h') ? "true" : "false");
	printf("\t5     : %s\n", ft_isalnum('5') ? "true" : "false");
	printf("\t*     : %s\n\n", ft_isalnum('*') ? "true" : "false");
}

void		ft_isascii_test(void)
{
	ft_puts("\tft_isascii tests : \n");
	printf("\tT      : %s\n", ft_isascii('T') ? "true" : "false");
	printf("\tspace  : %s\n", ft_isascii(' ') ? "true" : "false");
	printf("\tchar 2 : %s\n", ft_isascii('2') ? "true" : "false");
	printf("\tchar 5 : %s\n", ft_isascii('5') ? "true" : "false");
	printf("\t*      : %s\n", ft_isascii('*') ? "true" : "false");
	printf("\t258    : %s\n\n", ft_isascii(258) ? "true" : "false");
}

int			main(void)
{
	ft_puts("| ***************************************************** |");
	ft_puts("|                Testing ft_is* functions               |");
	ft_puts("| ***************************************************** |\n");
	ft_isalpha_test();
	ft_isdigit_test();
	ft_isalnum_test();
	ft_isascii_test();
	ft_puts("\tft_isprint tests : \n");
	printf("\tT      : %s\n", ft_isprint('T') ? "true" : "false");
	printf("\tspace  : %s\n", ft_isprint(' ') ? "true" : "false");
	printf("\tint 10 : %s\n", ft_isprint(10) ? "true" : "false");
	printf("\tchar 2 : %s\n", ft_isprint('2') ? "true" : "false");
	printf("\tchar 5 : %s\n", ft_isprint('5') ? "true" : "false");
	printf("\t*      : %s\n", ft_isprint('*') ? "true" : "false");
	printf("\t258    : %s\n\n", ft_isprint(258) ? "true" : "false");
	ft_puts("| ***************** END OF TESTS ********************* |");
	return (0);
}
