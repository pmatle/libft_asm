extern		_ft_isdigit
extern		_ft_isalpha

section		.text
	global	_ft_isalnum

_ft_isalnum:

check_numbers:

	call	_ft_isdigit
	cmp		rax, 1
	je		found

check_alpha:
	
	call	_ft_isalpha
	cmp		rax, 1
	je		found

not_found:
	mov		rax, 0
	ret
	
found:
	ret 
