section		.text
	global	_ft_isalpha

_ft_isalpha:

check_A:
	cmp		rdi, 65
	jl		check_a
	
check_Z:
	cmp		rdi, 90
	jle		found
	
check_a:
	cmp		rdi, 97
	jl		not_found
	
check_z:
	cmp		rdi, 122
	jle		found
	
found:
	mov		rax, 1
	ret
	
not_found:
	mov		rax, 0
	ret
