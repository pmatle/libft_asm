section		.text
	global	_ft_isprint

_ft_isprint:
	
check_min_printable:
	cmp		rdi, 32
	jl		not_found
	
check_max_printable:
	cmp		rdi, 126
	jle		found
	
not_found:
	mov		rax, 0
	ret
	
found:
	mov		rax, 1
	ret
	
