section		.text
	global	_ft_isascii

_ft_isascii:

check_min_ascii:
	cmp		rdi, 0
	jl		not_found

check_max_ascii:
	cmp		rdi, 127
	jle		found

not_found:
	mov		rax, 0
	ret

found:
	mov		rax, 1
	ret
