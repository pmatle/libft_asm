section		.text
	global	_ft_isdigit

_ft_isdigit:
	
check_0:
	cmp		rdi, 48
	jl		not_found

check_9:
	cmp		rdi, 57
	jle		found

not_found:
	mov		rax, 0
	ret

found:
	mov		rax, 1
	ret
