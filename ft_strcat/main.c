/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 08:52:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 15:35:15 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

void		tests(char *str1, char *str2)
{
	str1 = ft_memcpy(str1, "Hello World", 11);
	str2 = memcpy(str2, "Hello World", 11);
	ft_puts("Mine     : \n");
	ft_puts(str1);
	ft_puts("\nOriginal : \n");
	ft_puts(str2);
	str1 = ft_strcat(str1, " Phuti here");
	str2 = strcat(str2, " Phuti here");
	ft_puts("\nMine     : \n");
	ft_puts(str1);
	ft_puts("\nOriginal : \n");
	ft_puts(str2);
	str1 = ft_strcat(str1, " , telling a story like no other");
	str2 = strcat(str2, " , telling a story like no other");
	ft_puts("\nMine     : \n");
	ft_puts(str1);
	ft_puts("\nOriginal : \n");
	ft_puts(str2);
}

void		failing_tests(void)
{
	char	*str1;
	char	*str2;
	
	str1 = ft_strcat("Hello", " Phuti");
	str2 = strcat("Hello", " Phuti");
	ft_puts("\nMine     : \n");
	ft_puts(str1);
	ft_puts("\nOriginal : \n");
	ft_puts(str2);
	str1 = ft_strcat(NULL, " Phuti");
	str2 = strcat(NULL, " Phuti");
	ft_puts("\nMine     : \n");
	ft_puts(str1);
	ft_puts("\nOriginal : \n");
	ft_puts(str2);
}

int			main(void)
{
	char	*str1;
	char	*str2;

	str1 = (char *)malloc(sizeof(*str1) * 12);
	str2 = (char *)malloc(sizeof(*str2) * 12);
	ft_puts("| ***************************************************** |");
	ft_puts("|                   Testing ft_strcat                   |");
	ft_puts("| ***************************************************** |\n");
	tests(str1, str2);
	free(str1);
	free(str2);
	ft_puts("\n| ********************* END OF TEST ******************* |");
	return (0);
}
