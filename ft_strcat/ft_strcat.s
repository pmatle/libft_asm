extern		_ft_strlen

section		.text
	global	_ft_strcat

_ft_strcat:
	mov		rcx, -1
	mov		rbx, 0

loop:
	inc		rcx
	cmp		byte [rdi + rcx], 0
	jne		loop

combine_loop:
	mov		al, byte[rsi + rbx]
	mov		byte[rdi + rcx], al
	inc		rcx
	inc		rbx
	cmp		byte[rsi + rbx], 0
	jne		combine_loop

end_ft_strcat:
	mov		byte[rdi + rcx], 0
	mov		rax, rdi
	ret

