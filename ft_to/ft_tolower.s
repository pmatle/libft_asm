section		.text
	global	_ft_tolower

_ft_tolower:

check_A:
	cmp		rdi, 65
	jl		as_is

check_Z:
	cmp		rdi, 90
	jle		found

as_is:
	mov		rax, rdi
	ret

found:
	add		rdi, 32
	mov		rax, rdi
	ret

