section		.text
	global	_ft_toupper

_ft_toupper:

check_a:
	cmp		rdi, 97
	jl		as_is

check_z:
	cmp		rdi, 122
	jle		found

as_is:
	mov		rax, rdi
	ret

found:
	sub		rdi, 32
	mov		rax, rdi
	ret
