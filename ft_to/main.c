/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 08:52:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 16:07:44 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

int			main(void)
{
	ft_puts("| ***************************************************** |");
	ft_puts("|                Testing ft_to* functions               |");
	ft_puts("| ***************************************************** |");
	ft_puts(" ");
	ft_puts("\tft_tolower tests : \n");
	printf("\tT     : %c\n", ft_tolower('T'));
	printf("\ti     : %c\n", ft_tolower('i'));
	printf("\tH     : %c\n", ft_tolower('H'));
	printf("\t5     : %c\n", ft_tolower('5'));
	printf("\t*     : %c\n\n", ft_tolower('*'));
	ft_puts("\tft_upper tests : \n");
	printf("\tT     : %c\n", ft_toupper('T'));
	printf("\ti     : %c\n", ft_toupper('i'));
	printf("\th     : %c\n", ft_toupper('h'));
	printf("\t5     : %c\n", ft_toupper('5'));
	printf("\t*     : %c\n\n", ft_toupper('*'));
	ft_puts("| ***************** END OF TESTS ********************* |");
	return (0);
}
