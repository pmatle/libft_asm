
section		.text
	global	_ft_memchr

_ft_memchr:
	mov		rcx, rdx
	mov		rax, rsi
	cld
	repne	scasb
	je		found

exit:
	mov		rax, 0 
	ret

found:
	sub		rdi, 1
	mov		rax, rdi
	ret

