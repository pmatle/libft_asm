/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 12:01:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 14:38:00 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

void	ft_memchr_test(const char *str, int c, int len)
{
	char	*s1;
	char	*s2;
	char	*ans1;
	char	*ans2;

	s1 = (char *)malloc(sizeof(*s1) * 20);
	s2 = (char *)malloc(sizeof(*s2) * 20);
	s1 = ft_memcpy(s1, str, len);
	s2 = ft_memcpy(s2, str, len);
	ft_puts("Mine : \n");
	ans1 = ft_memchr(s1, c, len);
	ft_puts(ans1);
	ft_puts("\nOriginal : \n");
	ans2 = memchr(s2, c, len);
	ft_puts(ans2);
	ft_putstr("\n");
	free(s1);
	free(s2);
}

int		main(void)
{
	ft_puts("| ********************************************************* |");
	ft_puts("|                       Tests for ft_memchr                 |");
	ft_puts("| ********************************************************* |\n");
	ft_puts("| ****************** START OF PASSING TEST **************** |\n");
	ft_puts("\t\tPassing Tests : \n");
	ft_puts("\tTesting with letter l : \n");
	ft_memchr_test("Hello World!", 'l', 12);
	ft_puts("\tTesting with letter W : \n");
	ft_memchr_test("Ninja Worriors", 'W', 13);
	ft_puts("| ******************* END OF PASSING TEST ****************** |\n");
	ft_puts("| ****************** START OF FAILING TEST ***************** |\n");
	ft_puts("\t\tFailing Tests : \n");
	ft_puts("\tTesting with letter p : \n");
	ft_memchr_test("Hello World!", 'p', 12);
	ft_puts("\tTesting with letter -234 : \n");
	ft_memchr_test("Ninja Worriors", -234, 13);
	ft_puts("| ******************* END OF FAILING TEST ****************** |");
	return (0);
}
