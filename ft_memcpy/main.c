/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 08:52:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 15:01:54 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

void		ft_memcpy_test1(void)
{
	char	*s1;
	char	*s2;

	s1 = (char *)malloc(sizeof(*s1) * 12);
	s2 = (char *)malloc(sizeof(*s2) * 12);
	ft_puts("\n| *********** Dynamically Allocated string tests ********* |");
	s1 = ft_memcpy(s1, "Hello World", 11);
	s2 = memcpy(s2, "Hello World", 11);
	ft_puts("\nOriginal : \n");
	ft_puts(s1);
	ft_puts("\nMine : \n");
	ft_puts(s2);
	s1 = ft_memcpy(s1, "Ninja Worriors", 13);
	s2 = memcpy(s2, "Ninja Worriors", 13);
	ft_puts("\nOriginal : \n");
	ft_puts(s1);
	ft_puts("\nMine : \n");
	ft_puts(s2);
	free(s1);
	free(s2);
	ft_puts("\n| ****** END OF Dynamically Allocated string tests ******* |\n");
}

void		ft_memcpy_test2(void)
{
	char	*s1;
	char	*s2;

	ft_puts("\n| ******************** Static string tests ***************** |");
	s1 = ft_memcpy("Testing", "Hello World", 11);
	s2 = memcpy("Testing", "Hello World", 11);
	ft_puts("\nOriginal : \n");
	ft_puts(s1);
	printf("Mine : \n");
	ft_puts(s2);
	s1 = ft_memcpy("Bus Error", "Ninja Worriors", 13);
	s2 = memcpy("Bus Error", "Ninja Worriors", 13);
	ft_puts("\nOriginal : \n");
	ft_puts(s1);
	printf("Mine : \n");
	ft_puts(s2);
	ft_puts("\n| ***************** END OF Static string tests *********** |\n");
}

void		ft_memcpy_test3(void)
{
	char	*s1;
	char	*s2;

	s1 = (char *)malloc(sizeof(*s1) * 12);
	s2 = (char *)malloc(sizeof(*s2) * 12);
	ft_puts("\n| ****************** SERGV tests *************************** |");
	ft_puts("\t\tTesting if SERV :\n");
	s1 = ft_memcpy(s1, NULL, 11);
	s2 = memcpy(s2, NULL, 11);
	printf("Original : %s\n", s1);
	printf("Mine     : %s\n\n", s2);
	ft_puts("\n| ***************** END OF SERGV tests ******************* |\n");
}

int			main(void)
{
	ft_puts("| ********************************************************** |");
	ft_puts("|                        Testing ft_memcpy                   |");
	ft_puts("| ********************************************************** |\n");
	ft_memcpy_test1();
	ft_puts("| *********************** END OF TEST ********************** |");
	return (0);
}
