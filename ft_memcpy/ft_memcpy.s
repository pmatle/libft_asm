section		.text
	global	_ft_memcpy

_ft_memcpy:
	mov		rcx, rdx
	push	rdi

start_loop:
	lodsb
	stosb
	loop	start_loop
	cld
	rep		movsb
	pop		rax
	ret

