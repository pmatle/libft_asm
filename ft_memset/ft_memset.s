section		.text
	global	_ft_memset

_ft_memset:
	mov		rcx, rdx
	push	rdi
	mov		al, sil
	cld
	rep		stosb
	pop		rax
	ret

