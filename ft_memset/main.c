/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 08:52:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 15:16:38 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

void		failing_tests(void)
{
	ft_puts("\tTesting ft_memset with static string and \\0 :\n");
	printf("Original : %s\n", memset("Hello World", '\0', 11));
	printf("Mine     : %s\n\n", ft_memset("Hello World", '\0', 11));
	ft_puts("\t\tTesting ft_memcpy with NULL :\n");
	printf("Original : %s\n", memset(NULL, 'f', 11));
	printf("Mine     : %s\n", ft_memset(NULL, 'f', 11));
}

void		passing_tests(char *str1, char *str2)
{
	ft_puts("\tTesting ft_memset with malloced string and c :\n");
	ft_puts("Original : \n");
	ft_puts(memset(str1, 'c', 11));
	ft_puts("\nMine : \n");
	ft_puts(ft_memset(str2, 'c', 11));
	ft_puts("\n\tTesting ft_memset with malloced string and m :\n");
	ft_puts("\nOriginal : \n");
	ft_puts(memset(str1, 'm', 11));
	ft_puts("\nMine : \n");
	ft_puts(ft_memset(str2, 'm', 11));
	ft_puts("\n\tTesting ft_memset with malloced string and 0 :\n");
	ft_puts("\nOriginal : \n");
	ft_puts(memset(str1, 0, 11));
	ft_puts("\nMine : \n");
	ft_puts(ft_memset(str2, 0, 11));
}

int			main(void)
{
	char	*str1;
	char	*str2;

	str1 = (char *)malloc(sizeof(*str1) * 12);
	str2 = (char *)malloc(sizeof(*str2) * 12);
	ft_puts("| ***************************************************** |");
	ft_puts("|                   Testing ft_memcpy                   |");
	ft_puts("| ***************************************************** |\n");
	passing_tests(str1, str2);
	free(str1);
	free(str2);
	ft_puts("\n| ********************* END OF TEST ******************* |");
	return (0);
}
