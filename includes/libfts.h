/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libfts.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 07:35:39 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 13:59:41 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTS_H
# define LIBFTS_H

# include <stdlib.h>
# include <fcntl.h>
# include <unistd.h>
# include <string.h>
# include <stdio.h>

void		ft_bzero(void *s, size_t n);
char		*ft_strcat(char *s1, char const *s2);
int			ft_isalpha(int c);
int			ft_isdigit(int c);
int			ft_isalnum(int c);
int			ft_isascii(int c);
int			ft_isprint(int c);
int			ft_tolower(int c);
int			ft_toupper(int c);
int			ft_puts(const char *s);
void		ft_putstr(const char *s);
size_t		ft_strlen(const char *s);
void		*ft_memset(void *b, int c, size_t len);
void		*ft_memcpy(void *dst, const char *src, size_t n);
char		*ft_strdup(const char *s1);
void		*ft_memchr(const void *s, int c, size_t n);
int			ft_memcmp(const void *s1, const void *s2, size_t n);
int			ft_cat(int fd);
void		ft_swap_cp(char **p1, char **p2);
void		ft_swap_ip(int **p1, int **p2);
void		ft_swap_digits(void *ptr1, void *ptr2);
int			ft_max(int *array, int size);
int			ft_min(int *array, int size);

#endif
