/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 08:52:44 by pmatle            #+#    #+#             */
/*   Updated: 2018/06/28 15:39:38 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libfts.h"

void		failing_test(void)
{
	char	*str1;
	char	*str2;

	ft_puts("\n\nOriginal : \n");
	str1 = strdup(NULL);
	ft_puts(str1);
	ft_puts("\nMine   : \n");
	str2 = ft_strdup(NULL);
	ft_puts(str2);
}

int			main(void)
{
	char	*str1;
	char	*str2;

	ft_puts("| ***************************************************** |");
	ft_puts("|                   Testing ft_strdup                   |");
	ft_puts("| ***************************************************** |");
	ft_puts("\n");
	ft_puts("Original : \n");
	str1 = strdup("Hello World");
	ft_puts(str1);
	ft_puts("\nMine   : \n");
	str2 = ft_strdup("Hello World");
	ft_puts(str2);
	free(str1);
	free(str2);
	ft_puts("\nOriginal : \n");
	str1 = strdup("Testing testing testing");
	ft_puts(str1);
	ft_puts("\nMine   : \n");
	str2 = ft_strdup("Testing testing testing");
	ft_puts(str2);
	free(str1);
	free(str2);
	ft_puts("\n| ********************* END OF TEST ******************* |");
	return (0);
}
