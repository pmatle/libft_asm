extern		_malloc
extern		_ft_memcpy
extern		_ft_strlen

section		.text
	global	_ft_strdup

_ft_strdup:	
	push	rbp
	mov		rbp, rsp
	sub		rsp, 16

	mov		[rbp - 8], rdi

allocate_str:
	call	_ft_strlen
	mov		rdi, rax
	add		rdi, 1
	call	_malloc
	test	eax, eax
	jz		end

	mov		[rbp - 16], rax

copy_string:
	mov		rdi, [rbp - 8]
	call	_ft_strlen
	mov		rdi, [rbp - 16]
	mov		rsi, [rbp - 8]
	mov		rdx, rax
	call	_ft_memcpy
	
end:
	add		rsp, 16
	mov		rsp, rbp
	pop		rbp
	ret
	
